# VEngine Demo
A small demo made using [VEngine](https://github.com/kacperekjh/vengine).
The demo currently showcases 2 sample scenes. One 2D and one 3D.

## Requirements
- SDL2, SDL2_gfx and SDL2_tff libraries.
- C++20