#include "player.h"

void PlayerBase::Update()
{
    if (InputManager::IsHeld(SDL_SCANCODE_ESCAPE))
    {
        ObjectManager::GoToScene(ObjectManager::GetScene("menu"));
        return;
    }
}