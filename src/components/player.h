#ifndef _PLAYER_
#define _PLAYER_

#include <vengine>

class PlayerBase : public Component
{
protected:
    void Update() override;
};

#endif