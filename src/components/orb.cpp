#include "orb.h"

void Orb::Update()
{
    GetOwner()->rotation += rotation * GameLoop::GetDeltaTime();
}