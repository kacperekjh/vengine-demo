#include "2dplayer.h"
#include "../helper.h"

void Player2D::Update()
{
    PlayerBase::Update();
    float &rotation = GetOwner()->rotation.z;
    rotation = AngleLerp(rotation, targetRotation, rotationLerpT);

    Vector2 direction;
    if (InputManager::IsHeld(SDL_SCANCODE_W))
    {
        direction.y++;
    }
    if (InputManager::IsHeld(SDL_SCANCODE_S))
    {
        direction.y--;
    }
    if (InputManager::IsHeld(SDL_SCANCODE_A))
    {
        direction.x--;
    }
    if (InputManager::IsHeld(SDL_SCANCODE_D))
    {
        direction.x++;
    }
    direction.Normalize();
    direction *= speed * GameLoop::GetDeltaTime();
    GetOwner()->position += direction;
    if (direction != Vector2())
    {
        targetRotation = direction.ToAngleRad() - M_PI_2;
    }
}