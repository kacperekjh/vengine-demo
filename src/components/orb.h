#ifndef _ORB_
#define _ORB_

#include <vengine>

class Orb : public Component
{
protected:
    void Update() override;

public:
    Vector3 rotation;
};

#endif