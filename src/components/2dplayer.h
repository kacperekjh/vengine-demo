#ifndef _COMPONENTS_2DPLAYER_
#define _COMPONENTS_2DPLAYER_

#include "player.h"

class Player2D : public PlayerBase
{
private:
    const float speed = 3.5f;
    const float rotationLerpT = 0.15;
    float targetRotation = 0;

protected:
    void Update() override;
};

#endif