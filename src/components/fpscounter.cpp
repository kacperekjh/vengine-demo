#include "fpscounter.h"
#include <format>

void FPSCounter::Update()
{
    if (!text)
    {
        return;
    }
    text->SetText(std::format("{:.2f}", 1 / GameLoop::GetDeltaTime()));
}

void FPSCounter::SetTargetText(Pointer<UIText> text)
{
    FPSCounter::text = text;
}
