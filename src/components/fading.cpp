#include "fading.h"

void Fading::Update()
{
    time += GameLoop::GetDeltaTime();
    if (renderer) 
    {
        float alpha = (time - delay) / duration;
        alpha = 1 - alpha;
        alpha = alpha < 0 ? 0 : alpha > 1 ? 1 : alpha;
        for (auto &line : renderer->lines)
        {
            line.color.a = alpha * 255;
        }
    }
    else
    {
        renderer = GetOwner()->GetComponent<LineRenderer>();
    }
}

Fading::Fading(float delay, float duration) : delay(delay), duration(duration) {}