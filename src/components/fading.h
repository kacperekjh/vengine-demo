#ifndef _COMPONENTS_FADING_
#define _COMPONENTS_FADING_

#include <vengine>

class Fading : public Component
{
private:
    float time = 0;
    float delay;
    float duration;
    Pointer<LineRenderer> renderer = nullptr;

protected:
    void Update() override;

public:
    Fading(float delay, float duration);
};

#endif