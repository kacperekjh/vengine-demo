#ifndef _COMPONENTS_3DPLAYER_
#define _COMPONENTS_3DPLAYER_

#include "player.h"

class Player3D : public PlayerBase
{
private:
    const float speed = 3.5f;
    const float rotationSpeed = 180_deg;

protected:
    void Update() override;
};

#endif