#ifndef _FPSCOUNTER_H
#define _FPSCOUNTER_H

#include <vengine>

class FPSCounter : public Component
{
private:
    Pointer<UIText> text = nullptr;

protected:
    void Update() override;

public:
    void SetTargetText(Pointer<UIText> text);
};

#endif