#include "3dplayer.h"

void Player3D::Update()
{
    PlayerBase::Update();
    float rotation = 0;
    if (InputManager::IsHeld(SDL_SCANCODE_LEFT))
    {
        rotation -= rotationSpeed;
    }
    if (InputManager::IsHeld(SDL_SCANCODE_RIGHT))
    {
        rotation += rotationSpeed;
    }
    GetOwner()->rotation.y += rotation * GameLoop::GetDeltaTime();

    Vector3 direction;
    if (InputManager::IsHeld(SDL_SCANCODE_W))
    {
        direction.z++;
    }
    if (InputManager::IsHeld(SDL_SCANCODE_S))
    {
        direction.z--;
    }
    if (InputManager::IsHeld(SDL_SCANCODE_A))
    {
        direction.x--;
    }
    if (InputManager::IsHeld(SDL_SCANCODE_D))
    {
        direction.x++;
    }
    direction.Normalize();
    direction = direction.Rotate(GetOwner()->rotation);
    GetOwner()->position += direction * speed * GameLoop::GetDeltaTime();
}