#ifndef _SHUTTLE_FC98AF25835C80A6_
#define _SHUTTLE_FC98AF25835C80A6_

#include <vengine>

const std::vector<Line3D> shuttle = {
	std::vector<Vector3>{
		{0, 0, 0},
		{-0.3, -0.2, 0},
		{0, 0.5, 0},
		{0.3, -0.2, 0},
		{0, 0, 0}}};

#endif