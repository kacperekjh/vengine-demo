#include "helper.h"

std::vector<Line> AddColor(const std::vector<Line3D> &lines, Color color)
{
    std::vector<Line> colored;
    colored.reserve(lines.size());
    for (const auto &line : lines)
    {
        colored.push_back({line, color});
    }
    return colored;
}