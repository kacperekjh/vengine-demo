#include <vengine>
#include "components/2dplayer.h"
#include "components/3dplayer.h"
#include "components/fading.h"
#include "components/fpscounter.h"
#include "components/orb.h"
#include "helper.h"
#include "models/text.h"
#include "models/shuttle.h"
#include "models/orb.h"

struct Overlay
{
    Pointer<UIElement> root;
    Pointer<UIText> fps;
};

void CreateButton(Pointer<UIElement> parent, std::string_view text, std::function<void()> onClick)
{
    Pointer<UIButton> button = UIManager::CreateElement<UIButton>(parent, onClick);
    button->SetSize({100_pct, 50_px});
    button->SetMargin({0_px, 0_px, 0_px, 15_px});
    button->SetText(text);
    button->SetFont(FontManager::GetDefaultFont(26));
    button->SetColor({180, 180, 180});
    button->SetSelectedColor({255, 255, 255});
    button->SetTextColor({230, 230, 230});
}

Pointer<Scene> CreateMainMenu()
{
    Pointer<Scene> scene = new Scene();
    Pointer<UIContainer> container = scene->uiRoot = UIManager::CreateElement<UIContainer>(nullptr);
    container->SetDirection(UIDirection::TopToBottom);
    container->SetAlignment(UIAlignment::Center, UIAlignment::Begin);
    container->SetSize({100_pct, 100_pct});
    container->SetPadding({50_px, 50_px, 50_px, 50_px});

    Pointer<UIText> text = UIManager::CreateElement<UIText>(container);
    text->SetFont(FontManager::GetDefaultFont(52));
    text->SetText("VEngine Demo");
    text->SetSize({size_auto, size_auto});
    text->SetMargin({0_px, 0_px, 0_px, 100_px});

    Pointer<UIForm> buttons = UIManager::CreateElement<UIForm>(container);
    buttons->SetDirection(UIDirection::TopToBottom);
    buttons->SetSize({350_px, size_auto});

    CreateButton(buttons, "Start 2D demo", []() {
        ObjectManager::GoToScene(ObjectManager::GetScene("2d"));
    });
    CreateButton(buttons, "Start 3D demo", []() {
        ObjectManager::GoToScene(ObjectManager::GetScene("3d"));
    });
    CreateButton(buttons, "Exit", []() {
        GameLoop::EndLoop();
    });

    return scene;
}

Overlay CreateOverlay()
{
    Pointer<UIContainer> container = UIManager::CreateElement<UIContainer>(nullptr);
    container->SetSize({100_pct, size_auto});
    container->SetAlignment(UIAlignment::Begin, UIAlignment::Begin);
    container->SetPadding({5_px, 5_px, 5_px, 5_px});
    container->SetDirection(UIDirection::LeftToRight);

    Pointer<UIText> fps = UIManager::CreateElement<UIText>(container);
    fps->SetSize({50_px, size_auto});
    fps->SetFont(FontManager::GetDefaultFont(16));
    fps->SetTextAlignment(UIAlignment::Center, UIAlignment::Center);

    Pointer<UIText> text = UIManager::CreateElement<UIText>(container);
    text->SetSize({100_pct - 100_px, size_auto});
    text->SetFont(FontManager::GetDefaultFont(16));
    text->SetText("Click Escape to exit.");
    text->SetTextAlignment(UIAlignment::Center, UIAlignment::Center);

    return {container, fps};
}

Pointer<Scene> Create2DDemo()
{
    Pointer<Scene> scene = new Scene();
    auto camera = ObjectManager::CreateObject(scene);
    camera->position.z = -10;
    camera->AddComponent<Camera>();

    auto player = ObjectManager::CreateObject(scene);
    player->AddComponent<Player2D>();
    player->AddComponent<LineRenderer>(AddColor(shuttle, Color(255, 120, 120)));

    Overlay overlay = CreateOverlay();
    scene->uiRoot = overlay.root;

    auto fps = player->AddComponent<FPSCounter>();
    fps->SetTargetText(overlay.fps);

    return scene;
}

Pointer<Scene> Create3DDemo()
{
    Pointer<Scene> scene = new Scene();
    auto player = ObjectManager::CreateObject(scene);
    player->AddComponent<Player3D>();
    auto camera = player->AddComponent<Camera>();
    camera->projection = Perspective;

    auto text = ObjectManager::CreateObject(scene);
    text->position = {-1.5f, 2.5f, 10};
    text->AddComponent<LineRenderer>(AddColor(GetText2(), {255, 255, 255}));

    auto orb = ObjectManager::CreateObject(scene);
    orb->position = {0, 1, 10};
    auto orbc = orb->AddComponent<Orb>();
    orbc->rotation = {0, 90_deg, 0};
    orb->AddComponent<LineRenderer>(AddColor(::orb, {130, 130, 255}));

    auto orbLabel = ObjectManager::CreateObject(scene);
    orbLabel->position = {1, 1, 10};
    orbLabel->AddComponent<LineRenderer>(AddColor(GetText3(), {255, 255, 255}));

    Overlay overlay = CreateOverlay();
    scene->uiRoot = overlay.root;

    auto fps = player->AddComponent<FPSCounter>();
    fps->SetTargetText(overlay.fps);

    return scene;
}

void Scenes::CreateScenes()
{   auto menu = CreateMainMenu();
    ObjectManager::AddScene("menu", menu);
    ObjectManager::AddScene("2d", Create2DDemo());
    ObjectManager::AddScene("3d", Create3DDemo());
    ObjectManager::GoToScene(menu);
}